<?php

namespace App\Exports;

use App\Models\voluntarios;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ExportEmployes implements FromCollection,WithHeadings
{

	/**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
                   ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

		$voluntarios = DB::table('voluntarios')->get();    	//dd(employes::all());
		return $voluntarios;
        //return employes::all();
    }
}
