<?php

namespace App\Jobs;
use app\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendAccountActivationEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle(Mailer $mailer)
    {
        \Log::info("ENVIANDO EMAIL A {$this->user->email}.....ID: {$this->user->id}." );

        $mailer->send(new \App\Mail\AccountActivationEmail($this->user));
    }
}
