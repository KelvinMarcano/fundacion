<?php

namespace App\Http\Controllers\ImportExcel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\ImportEmployes;
use App\Exports\ExportEmployes;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Models\voluntarios;

class ImportExcelController extends Controller
{
    //
     public function index()
    {   

        $voluntarios = voluntarios::orderBy('created_at','DESC')->get();

        

        //dd($employes2);
        return view('import_excel.index', compact('voluntarios'));
    }

    public function import(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);
        Excel::import(new ImportEmployes, request()->file('import_file'));
        return back()->with('success', 'Employes imported successfully.');
    }

}
