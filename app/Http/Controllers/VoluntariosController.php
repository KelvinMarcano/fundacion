<?php

namespace App\Http\Controllers;

use DB;
use App\Models\voluntarios;
use App\Models\parroquia;
use App\Models\estado;
use App\Models\pais;
use App\Models\municipio;
use App\Models\localidad;
use App\Models\profesiones;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Illuminate\Support\Facades\Auth;

class VoluntariosController extends Controller
{

    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $voluntarios = (new voluntarios());
          $data['voluntarios'] = DB::table('voluntario')
        ->selectRaw('voluntario.id as id, voluntario.nombre,voluntario.apellido, voluntario.sexo, TIMESTAMPDIFF(YEAR,voluntario.fecha_nacimiento,CURRENT_DATE) as fecha_nacimiento, voluntario.num_telefono,voluntario.email')
        ->orderBy('id','DESC')->get();
       // dd($data);

         $data['profesiones'] = DB::table('profesiones')
        ->select('profesiones.id as id', 'profesiones.descripcion1 as descripcion1')
        ->get();
        
        $data['estado'] = DB::table('estado')->orderBy('id','ASC')->get();

        $data['paises'] = DB::table('pais')->orderBy('id','ASC')->get();

        $data['municipio'] = DB::table('municipio')->orderBy('id','ASC')->get();

        $data['parroquia'] = DB::table('parroquia')->orderBy('id','ASC')->get();

        $data['nacionalidad'] = DB::table('pais')
        ->select('pais.id as id','pais.nombre as nombre', 'pais.nacionalidad as nacionalidad')
         ->where('pais.nacionalidad', '<>', '')
        ->get();
        //  $data['municipio'] = DB::table('municipio')
        // ->select('municipio.id as id','municipio.nombre as nombre')
        // ->get();
        //  $data['parroquia'] = DB::table('parroquia')
        // ->select('parroquia.id as id','parroquia.nombre as nombre')
        // ->get();
        //  $data['localidad'] = DB::table('localidad')
        // ->select('localidad.id as id','localidad.nombre as nombre')
        // ->get();

        return view('registro.voluntarios', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new voluntarios();
            $model = $model->find($request->id);

            if(empty($model)) $model = new voluntarios();

            $model->saveData($request->all());

             $mail = new MailController();
             $mail->sendMailVoluntario($request);

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'voluntarios');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'voluntarios');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'voluntarios');
            Resp::status($request,"error",$e->getMessage(),'save '.'voluntarios');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new voluntarios();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'voluntarios');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'voluntarios', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'voluntarios');
        }
    }

}


