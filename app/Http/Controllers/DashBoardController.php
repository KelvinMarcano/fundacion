<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;


class DashBoardController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();

       return view('home', $data);
    }

}