<?php

namespace App\Http\Controllers\ExportExcel;

use DB;
use App\Models\voluntarios;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\ExportEmployes;
use App\Exports\ExportAbsences;
use Maatwebsite\Excel\Facades\Excel;

class ExportExcelController extends Controller
{
	  /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
    	return Excel::download(new ExportEmployes, 'voluntarios.xlsx');
    	//Excel::download(new ExportEmployes, 'employes.xlsx');
        //return back()->with('success', 'Employes export successfully.');
    }

     /**
    * @return \Illuminate\Support\Collection
    */
    public function exportAbsences() 
    {
    	return Excel::download(new ExportAbsences, 'absences.xlsx');
    	//Excel::download(new ExportEmployes, 'employes.xlsx');
        //return back()->with('success', 'Employes export successfully.');
    }

}