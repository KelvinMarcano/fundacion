<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Response as Resp;
use Session;
use JWTFactory;
use JWTAuth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $validator = $this->validateLogin($request->all());

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach ($errors as $error) {
                $err .= $ctn++ . ')' . $error . '\n';
            }
            Resp::status($request,"warning",$err,"Login");
            return back()->withInput($request->all());
        }

        $user = new User();
                
        $user = $user->where('email',$request->email)->first();
        //$user = $user->where('username',$request->email)->first();

        if (empty($user)) {
            Resp::status($request,"warning",'Unregistered user.',"Login");
            return redirect()->back()->withInput($request->all());
        }

        if (!$user->state) {
            Resp::status($request,"warning",'User blocked.',"Login");
            return redirect()->back()->withInput($request->all());
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }


    protected function validateLogin(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required',
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt($this->credentials($request), $request->filled('remember'));
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        //$this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user()) ?: redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
        //
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        Resp::status($request,"warning",'Wrong password.',"Login");
        return redirect()->back()->withInput($request->all());
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function logout(Request $request) {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return $this->loggedOut($request) ?: redirect('auth');
        //Session::flush();
        //Auth::logout();
        //return Redirect('auth');
    }

      protected function loggedOut(Request $request)
    {
        //
    }

    public function me()
    {
        return response()->json(auth()->user());
    }


}
