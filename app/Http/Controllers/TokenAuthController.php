<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TokenAuthController extends Controller
{
   public function signup(CreateUserRequest $request)
    {

        $token = JWTAuth::fromUser($user);
        
        // Enviamos el mailable a la cola de trabajo
        $this->dispatch(new \App\Jobs\SendAccountActivationEmail($user));

        return response()->success(compact('token'));
    }

    public function getConfirmation($token)
    {
        try
        {
            $user = User::where('registration_token', $token)->firstOrFail();
 
            $user->registration_token = null;
            $user->active             = true;
            $user->save();
 
            return response()->success('email_confirmed', 200);
        } catch (Exception $e) {
            return response()->error('error_verifying_your_email', $e->getStatusCode());
        }
    }

}
