<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function test(Request $request)
    {
        //dd($request->server('HTTP_HOST'));
        try{
            $emails = [
                "kelmarcano@gmail.com"
            ];
            $data['body'] = "asdasdasdasd";
            $data['name'] = "Kelvin";
            Mail::send('mails.test', $data, function ($message) use ($emails) {
                $message->from(env('MAIL_USERNAME'), 'Witmakers');
                $message->to($emails)->subject('Email de pruebas.')
                //->bcc('eikerdjforever@gmail.com')
                ->getHeaders()
                ->addTextHeader('Disposition-Notification-To','eikerdjforever@gmail.com')
                ->addTextHeader('Disposition-Notification-To','eikerdjforever@gmail.com');
            });
            dd("enviado");
        }catch (\Exception $e){
            dd($e->getMessage());
        }

    }

    public function sendMailUsuario(Request $request)
    {
          // dd($request->email);
        try{
            $Id =  $request->id;
            $nombre =  $request->name;
            $data['id'] = $Id;
            $data['name'] = $nombre;

            $url='www.fundacionahora.com';
            $correo = $request->email;

            Mail::send('mails.usuario', $data, function ($message) use ($correo, $url) {
                $message->from($correo, 'Fundacion Ahora');
                $message->to($correo)->subject('Activacion de Usuario');
            });

            return redirect()->back();
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

     public function sendMailVoluntario(Request $request)
    {
        try{
            $Id =  $request->id;
            $nombre =  $request->name;
            $data['id'] = $Id;
            $data['name'] = $nombre;

            $url='www.fundacionahora.com';
            $correo = $request->email;

            Mail::send('mails.voluntario', $data, function ($message) use ($correo, $url) {
                $message->from($correo, 'Fundacion Ahora');
                $message->to($correo)->subject('Activacion de Voluntario');
            });
            
            return redirect()->back();
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
