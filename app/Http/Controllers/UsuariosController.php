<?php

namespace App\Http\Controllers;

use App\Models\roles;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class UsuariosController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function roles()
    {
        $data = array();
        $data['roles'] = (new roles())->all();
        return view('configuracion.roles', $data);
    }

    public function registration(){
        return view('registro.register');
    }

    public function users()
    {
        $data = array();
        $data['users'] = (new User())->all();
        $data['roles'] = (new roles())->where('rol_active',1)->get();
        //dd($data);
        return view('configuracion.users', $data);
    }

    public function save(Request $request)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($request->id);

            if(empty($model)) $model = new $className();

            $model->saveData($request->all());
          
            $mail = new MailController();
            $mail->sendMailUsuario($request);

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.$request->table);

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.$request->table);
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.$request->table);
            Resp::status($request,"error",$e->getMessage(),'save '.$request->table);
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.$request->table);

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.$request->table,$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.$request->table);
        }
    }

        public function saveRegister(Request $request)
    {
        try{
          //  dd("llego");
            $model = new User();
            $model = $model->find($request->id);
           // dd($request->all());
            if(empty($model)) $model = new User();

            $model->saveDataR($request->all());
          
            $mail = new MailController();
            $mail->sendMailUsuario($request);

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.$request->table);

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.$request->table);
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.$request->table);
            Resp::status($request,"error",$e->getMessage(),'save '.$request->table);
            return redirect()->back();
        }
    }
}
