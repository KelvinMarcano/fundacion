<?php

namespace App\Imports;

use App\Models\voluntarios;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;

class ImportEmployes implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new voluntarios([
            //

            //dd($this->transformDate($row[6]));
            'id' => @$row[0],
            'nombre' => @$row[1],
            'apellido' => @$row[2], 
            'doc_identidad' => @$row[3],
            'num_doc_identidad' => @$row[4], 
            'sexo' => @$row[5], 
            'fecha_nacimiento' => $this->transformDate($row[6]),
            'nacionalidad' => @$row[7], 
            'num_telefono' => @$row[8],
            'edo_civil' => @$row[9],
            'nivel_estudio' => @$row[10].
            'profesion' => @$row[11],
            'situacion_laboral' => @$row[12]
            



        ]);
    }


    /**
     * Transform a date value into a Carbon object.
     *
     * @return \Carbon\Carbon|null
     */
    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
