<?php

namespace App\Mail;
use app\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountActivationEmail extends Mailable
{
   use Queueable, SerializesModels;
    // las propiedades publicas serán
    // visibles desde la vista del email
    public $user;
    public $url;
 
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->url = route('account_activation',$user->registration_token);
    }
 
    public function build()
    {
        return $this->view('emails.activation')
                    ->to($this->user->email,$this->user->name)
                    ->subject('Activa tu cuenta!');
    }
}
