<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Notifications\ResetPassword;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable = ['email','name','password'];
    protected $hidden = [ 
      'registration_token'
    ];
    protected $table = "users";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function role()
    {
        return $this->hasOne('App\Models\roles','id','rol_id')->first();
    }

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
       // dd("user");
        $data['password'] = (!empty($data['password'])) ? Hash::make($data['password']) : $this->password;
        $this->Validator($data);
        $this->email = (!empty($data['email'])) ? $data['email'] : $this->email;
        $this->name = (!empty($data['name'])) ? $data['name'] : $this->name;
        $this->password = $data['password'];
        $this->rol_id = (!empty($data['rol_id'])) ? $data['rol_id'] : $this->rol_id;
        return parent::save();
    }

    public function saveDataR(array $data = []){
       // dd("user");
        $data['password'] = (!empty($data['password'])) ? Hash::make($data['password']) : $this->password;
        $this->Validator($data);
        $this->email = (!empty($data['email'])) ? $data['email'] : $this->email;
        $this->name = (!empty($data['name'])) ? $data['name'] : $this->name;
        $this->password = $data['password'];
        $this->rol_id = (!empty($data['rol_id'])) ? $data['rol_id'] : $this->rol_id;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'email' => $required.',email',
                'name' => 'required',
                'password' => 'required',
                'rol_id' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'email' =>  $required.',email,'.$data['id'].','.$this->primaryKey,
                'name' => 'required',
                'password' => 'required',
                'rol_id' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // Función copiada del archivo:
     // vendor\laravel\framework\src\Illuminate\Auth\Passwords\CanResetPassword.php     
     /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }


}
