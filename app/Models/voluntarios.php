<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class voluntarios extends Model
{
    protected $table = 'voluntario';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : $this->nombre;
		$this->apellido = (!empty($data['apellido'])) ? $data['apellido'] : $this->apellido;
		$this->doc_identidad = (!empty($data['doc_identidad'])) ? $data['doc_identidad'] : $this->doc_identidad;
		$this->num_doc_identidad = (!empty($data['num_doc_identidad'])) ? $data['num_doc_identidad'] : $this->num_doc_identidad;
		$this->sexo = (!empty($data['sexo'])) ? $data['sexo'] : $this->sexo;
		$this->fecha_nacimiento = (!empty($data['fecha_nacimiento'])) ? $data['fecha_nacimiento'] : $this->fecha_nacimiento;
		$this->nacionalidad_id = (!empty($data['nacionalidad'])) ? $data['nacionalidad'] : $this->nacionalidad_id;
		$this->num_telefono = (!empty($data['num_telefono'])) ? $data['num_telefono'] : $this->num_telefono;
		$this->edo_civil = (!empty($data['edo_civil'])) ? $data['edo_civil'] : $this->edo_civil;
		$this->nivel_estudio = (!empty($data['nivel_estudio'])) ? $data['nivel_estudio'] : $this->nivel_estudio;
		$this->profesion_id = (!empty($data['profesion'])) ? $data['profesion'] : $this->profesion_id;
		$this->situacion_laboral = (!empty($data['situacion_laboral'])) ? $data['situacion_laboral'] : $this->situacion_laboral;
		$this->pais_id= (!empty($data['pais'])) ? $data['pais'] : $this->pais_id;
        $this->estado_id= (!empty($data['estado'])) ? $data['estado'] : $this->estado_id;
		$this->municipo_id = (!empty($data['municipio'])) ? $data['municipio'] : $this->municipo_id;
		$this->parroquia_id = (!empty($data['parroquia'])) ? $data['parroquia'] : $this->parroquia_id;
		$this->direccion = (!empty($data['direccion'])) ? $data['direccion'] : $this->direccion;
		$this->cargo_comunidad = (!empty($data['cargo_comunidad'])) ? $data['cargo_comunidad'] : $this->cargo_comunidad;
		$this->email = (!empty($data['email'])) ? $data['email'] : $this->email;
        $this->instagram= (!empty($data['instagram'])) ? $data['instagram'] : $this->instagram;
        $this->facebook = (!empty($data['facebook'])) ? $data['facebook'] : $this->facebook;
        $this->twitter = (!empty($data['twitter'])) ? $data['twitter'] : $this->twitter;
        $this->modalidad_ayuda = (!empty($data['modalidad_ayuda'])) ? $data['modalidad_ayuda'] : $this->modalidad_ayuda;
        $this->movilidad = (!empty($data['movilidad'])) ? $data['movilidad'] : $this->movilidad;
        $this->comentarios = (!empty($data['comentario'])) ? $data['comentario'] : $this->comentarios;
        $this->acepto_terminos = (!empty($data['acepto_terminos'])) ? 1 : 0;
        return parent::save();
    }

    public function updateVoluntariosData(array $data = []){
        //$this->Validator($data);
        $this->users_id = (!empty($data['users_id'])) ? $data['users_id'] : $this->users_id;
        
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'nombre' => $required.',nombre',
                'apellido' => 'required',
                'doc_identidad' => 'required',
                'num_doc_identidad' => 'required',
                'sexo' => 'required',
                'fecha_nacimiento' => 'required',
                'nacionalidad' => 'required',
                'edo_civil' => 'required',
                'nivel_estudio' => 'required',
                'profesion' => 'required',
                'situacion_laboral' => 'required',
                'pais' => 'required',
                'estado' => 'required',
                'municipio' => 'required',
                'parroquia' => 'required',
                'direccion' => 'required',
                'email' => 'required',
                'facebook' => 'required',

            ]);
        }else{
            $validator = Validator::make($data, [
                'nombre' =>  $required.',nombre,'.$data['id'].','.$this->primaryKey,
                'apellido' => 'required',
                'doc_identidad' => 'required',
                'num_doc_identidad' => 'required',
                'sexo' => 'required',
                'fecha_nacimiento' => 'required',
                'nacionalidad' => 'required',
                'edo_civil' => 'required',
                'nivel_estudio' => 'required',
                'profesion' => 'required',
                'situacion_laboral' => 'required',
                'pais' => 'required',
                'estado' => 'required',
                'municipio' => 'required',
                'parroquia' => 'required',
                'direccion' => 'required',
                'email' => 'required',
                'facebook' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

   
}