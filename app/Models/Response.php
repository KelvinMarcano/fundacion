<?php

namespace App\Models;

class Response
{
    public static function status($request, $status,$msg = "",$accion = ""){
        //$status = success,error,warning,info,question
        //$status1 = Éxito,Error,Alerta!,Importante!,Confirmar
        $request->session()->flash('status', array('status' => $status,'title' => self::mensaje($status),'msg' =>$msg,'accion' =>$accion));
        return true;
    }

    public static function statusJson($request, $status,$msg = "",$accion = "",$data = null)
    {
        $code = 200;
        switch ($status){
            case "error" : {
                $code = 500;
                break;
            };
            case "warning" : {
                $code = 500;
                break;
            };
        }
        return \Response::json(array('status' => $status,'title' => self::mensaje($status),'msg' =>$msg,'accion' =>$accion,'data' => $data,'code' => $code),$code);
    }

    public static function mensaje($status){
        $message = "";
        switch ($status){
            case "question": {
                $message = "Confirm";
                break;
            };
            case "info": {
                $message = "Important!";
                break;
            };
            case "warning": {
                $message = "Alert!";
                break;
            };
            case "error": {
                $message = "Error";
                break;
            };
            case "success": {
                $message = "Success";
                break;
            };
        }
        return $message;
    }
}