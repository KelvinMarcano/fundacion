<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class roles extends Model
{
    protected $table = "roles";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function users()
    {
        return $this->hasMany('App\Models\User','rol_id','id');
    }

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->name = (!empty($data['name'])) ? $data['name'] : $this->name;
        $this->rol_active = (!empty($data['rol_active'])) ? 1 : 0;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'name' => $required.',name',
            ]);
        }else{
            $validator = Validator::make($data, [
                'name' =>  $required.',name,'.$data['id'].','.$this->primaryKey,
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

}