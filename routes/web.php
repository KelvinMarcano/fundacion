<?php

Route::get('auth', 'Auth\LoginController@index');
Route::post('auth', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
//Route::get('auth/register', 'Auth\RegisterController@registration');
Route::get('usersR', 'UsuariosController@registration');
Route::post('usersR', 'UsuariosController@saveRegister');
Route::post('auth/register', 'Auth\RegisterController@postRegistration');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('test-envio',function(){
    $data['body'] = "Esto es una prueba";
    $data['name'] = "Fundacion Ahora";
    $emails='kelmarcano@gmail.com';
    //$emails2='kelmarcano_5@hotmail.com';
    $url='www.fundacionahora.com';
//dd(env('MAIL_USERNAME'));
    //dd($data);
    Mail::send('mails.test', $data, function ($message) use($emails, $url){
                $message->from($emails, 'Fundacion Ahora');
                $message->to($emails)->subject($url);
               
            });

           // dd("enviado");
});

// Verify Account by email
// Route::get('account/activation/{token}', [
//     'uses' => 'TokenAuthController@getConfirmation',
//     'as'   => 'confirmation',
// ]);

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'VoluntariosController@index');

    Route::get('roles', 'UsuariosController@roles');
    Route::post('roles', 'UsuariosController@save');
    Route::delete('roles/{id}', 'UsuariosController@delete');

    Route::get('users', 'UsuariosController@users');
    Route::post('users', 'UsuariosController@save');
    Route::delete('users/{id}', 'UsuariosController@delete');


    Route::get('voluntarios', 'VoluntariosController@index');
    Route::post('voluntarios', 'VoluntariosController@save');
    Route::delete('voluntarios/{id}','VoluntariosController@delete');

    Route::get('sendmail','MailController@sendMailUsuario');

    Route::get('import-excel', 'ImportExcel\ImportExcelController@index');
    Route::post('import-excel', 'ImportExcel\ImportExcelController@import');

    Route::get('export-excel', 'ExportExcel\ExportExcelController@export')->name('export-excel');

     Route::get('dashboard', 'DashBoardController@index');
});

route::get('clear-all-cache', function (){
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return \Illuminate\Support\Facades\Response::json(['data'=>'all cache clear']);
});