@extends('layouts.backend')

@section('content')
<body>
    
<div class="container">
    <div class="card mt-4">
         <div class="card-body">Export Excel File Example</div>
          <a class="btn btn-success" href="{{ route('export-excel') }}">Export Data</a>
            <!-- <form action="{{ url('export-excel') }}" method="post" name="exportform" enctype="multipart/form-data">
                @csrf            
              <button class="btn btn-primary">Download File</button>
            </form> -->
             <div>
                 <h2 style='background-color:#FF0000;'>Note: Before import database data first export one file example</h2>
             </div>
        <br>
        <br>

        <div class="card-header">
            Import Excel to database - 
        </div>
            @if ($errors->any())
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
        <div class="card-body">
            <form action="{{ url('import-excel') }}" method="POST" name="importform" enctype="multipart/form-data">
                @csrf
                <input type="file" name="import_file" class="form-control">
                 <br>
                <br>
                <button class="btn btn-success">Import File</button>
                <br>
            </form>
        </div>
    </div>
    <br>
    <br>
    <div class="panel panel-default">
      <div class="panel-heading">
       <h3 class="panel-title">Data de Voluntarios</h3>
      </div>
        <div class="panel-body">
            
          </div>
            </div>
</div>
 
</div>
    
</body>
@endsection
