@extends('layouts.simple')
@push('scripts')
    <title>App Shedule | Registro</title>
@endpush

@section('content')

    <div class="bg-image" style="background-image: url('{{ asset('images/logo3.gif') }}');">
        <div class="row no-gutters justify-content-center bg-black-75">
            <!-- Main Section -->
            <div class="hero-static col-md-6 d-flex align-items-center bg-white">
                <div class="p-3 w-100">
                    <!-- Header -->
                    <div class="mb-3 text-center">
                        <a class="text-success font-size-h1" href="app/auth">
                            <img src="{{ asset('images/logo3.gif') }}" width="200" height="200">
                        </a>
                        <p class="text-uppercase font-size-sm text-muted">Crear nueva cuenta</p>
                    </div>
                    <!-- END Header -->

                    <!-- Sign Up Form -->
                    <!-- jQuery Validation (.js-validation-signup class is initialized in js/pages/op_auth_signup.min.js which was auto compiled from _es6/pages/op_auth_signup.js) -->
                    <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <div class="row no-gutters justify-content-center">
                        <div class="col-sm-8 col-xl-6">
                            <form method="post" action="usersR">
                                {{ csrf_field() }}
                                <div class="py-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg form-control-alt" id="name" name="name" placeholder="Nombre">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-lg form-control-alt" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg form-control-alt" id="password" name="password" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg form-control-alt" id="password-confirm" name="password-confirm" placeholder="Password Confirm">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-success" style="background-color: #3d808c">
                                        <i class="fa fa-fw fa-plus mr-1"></i> Registrarse
                                    </button>
                                    <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                                        <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="/auth">
                                            <i class="fa fa-sign-in-alt text-muted mr-1"></i> Iniciar sesión
                                        </a>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Sign Up Form -->
                </div>
            </div>
            <!-- END Main Section -->
        </div>
    </div>
    <!-- END Page Content -->
@endsection
