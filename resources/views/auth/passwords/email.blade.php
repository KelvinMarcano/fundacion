@extends('layouts.simple')
@push('scripts')
    <title>Fundacion Ahora</title>
@endpush

@section('content')

    <div class="bg-image" style="background-image: url('{{ asset('public/images/logo3.gif') }}');">
        <div class="row no-gutters justify-content-center bg-black-75">
            <!-- Main Section -->
            <div class="hero-static col-md-6 d-flex align-items-center bg-white">
                <div class="p-3 w-100">
                    <!-- Header -->
                    <div class="mb-3 text-center">
                        <a class="text-success font-size-h1" href="app/auth">
                            <img src="{{ asset('public/images/logo3.gif') }}" width="200" height="200">
                        </a>
                        <p class="text-uppercase font-size-sm text-muted">Recuperacion de Contraseña</p>
                    </div>

                    <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- END Header -->

                    <!-- Sign Up Form -->
                    <!-- jQuery Validation (.js-validation-signup class is initialized in js/pages/op_auth_signup.min.js which was auto compiled from _es6/pages/op_auth_signup.js) -->
                    <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <div class="row no-gutters justify-content-center">
                        <div class="col-sm-8 col-xl-6">
                                <form class="js-validation-signup" action="password/email" method="post">
                                 {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-8 control-label">E-Mail Address</label>

                                        <div class="col-md-12">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Send Password Reset Link
                                            </button>
                                           <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                                                <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="/auth">
                                                    <i class="fa fa-sign-in-alt text-muted mr-1"></i> Iniciar sesión
                                                </a>
                                                </p>
                                        </div>

                                    </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END Sign Up Form -->
                </div>
            </div>
            <!-- END Main Section -->
        </div>
    </div>
    <!-- END Page Content -->
@endsection
