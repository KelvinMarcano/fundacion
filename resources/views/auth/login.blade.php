@extends('layouts.simple')
@push('scripts')
    <title>Fundacion Ahora | Login</title>
@endpush

@section('content')
    <!-- Page Content -->
    <div class="bg-image" >
        <div class="row no-gutters bg-primary-op">
            <!-- Main Section -->
            <div class="hero-static col-md-6 d-flex align-items-center bg-white">
                <div class="p-3 w-100">
                    <!-- Header -->
                    <div class="mb-3 text-center">
                        <a class="font-w500 font-size-h1" href="/">
                            <img src="{{ asset('images/logo3.gif') }}" width="200" height="200">               
                        </a>
                    </div>
                    <!-- END Header -->

                    <!-- Sign In Form -->
                    <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.min.js which was auto compiled from _es6/pages/op_auth_signin.js) -->
                    <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <div class="row no-gutters justify-content-center">
                        <div class="col-sm-8 col-xl-6">
                            <form class="js-validation-signin" method="POST" action="auth">
                                {{ csrf_field() }}
                                <div class="py-3">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-lg form-control-alt" id="login-username" name="email" placeholder="Usuario" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg form-control-alt" id="login-password" name="password" placeholder="Contraseña" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary" style="background-color: #3d808c">
                                        <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Entrar
                                    </button>
                                    <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                                        <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="password/reset">
                                            <i class="fa fa-exclamation-triangle text-muted mr-1"></i> Recuperar contraseña
                                        </a>
                                        <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="usersR">
                                            <i class="fa fa-plus text-muted mr-1"></i> Registrarse
                                        </a>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Sign In Form -->
                </div>
            </div>
            <!-- END Main Section -->

            <!-- Meta Info Section -->
            <div class="hero-static col-md-6 d-none d-md-flex align-items-md-center justify-content-md-center text-md-center" style="align-items: flex-end !important;">
                <div class="p-3">
                    <p class="display-4 text-white mb-3" style="padding-top: 150px">
                                  DANDO UNA MANO A LOS MÁS NECESITADOS EN VENEZUELA
                                </p>
                                <p class="font-size-lg text-white-75 mb-0" style="padding-top: 150px">
                                    "Copyright &copy; <span class="js-year-copy">2020"</span>
                                </p>
                </div>
            </div>
            <!-- END Meta Info Section -->
        </div>
    </div>
    <!-- END Page Content -->
@endsection