@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Roles</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white" data-toggle="modal" data-target="#roleModal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Nuevo Rol
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center" style="width: 80px;">#</th>
                  <th class="text-center">Roles</th>
                  <th class="text-center"># Usuario</th>
                  <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Estatus</th>
                  <th class="text-center">Acciones</th>
              </tr>
          </thead>
          <tbody>
            @foreach($roles as $rol)
              <tr class="text-center" id="trrol_{{ $rol->id }}">
                <td class="text-center">{{ $rol->id }}</td>
                <td class="font-w600">{{ $rol->name }}</td>
                <td class="font-w600">{{ $rol->users()->count() }}</td>
                <td class="d-none d-sm-table-cell">
                  @if($rol->rol_active)
                    <span class="badge badge-success">Activo</span>
                  @else
                    <span class="badge badge-danger">Inactivo</span>
                  @endif
                </td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#roleModal" data-whatever="@getbootstrap" data-id="{{ $rol->id }}" title="Modificar">
                      <i class="si si-pencil text-white"></i>
                     
                  </button>
                  @if(!$rol->users()->count())
                    <a type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $rol->id }}" title="Eliminar">
                      <i class="si si-close text-white"></i>
                     
                    </a>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Nuevo Rol</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="roles" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <input type="hidden" name="table" value="roles">
            <div class="form-group">
              <label for="role" class="col-form-label">Rol:</label>
              <input type="text" class="form-control" id="name" name="name" required placeholder="">
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Activo</label>
                <select class="form-control" id="rol_active" name="rol_active" required placeholder="seleccione">
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
            </div>
          </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Salir</a>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload');
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add new role");
      $('#id').val('');
      $('#rol_active').val('');
      $('#name').val('');
    });

    $('.edit').on('click',async function () {
      $('#label').html("Role edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let roles = @json($roles);
      roles.forEach((element) =>{
          if(id == element.id){
              $('#rol_active').val(element.rol_active);
              $('#name').val(element.name);
          }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`roles/${id}`,'delete',{table : 'roles'});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }
  </script>
@endsection