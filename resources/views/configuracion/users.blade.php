@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Usuarios</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#roleModal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Nuevo Usuario
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Nombre</th>
                  <th class="text-center">Correo</th>
                  <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Rol</th>
                  <th class="d-none d-sm-table-cell text-center" style="width: 10%;">Estatus</th>
                  <th class="text-center" style="width: 20%;">Acciones</th>
              </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
              <tr class="text-center" id="trrol_{{ $user->id }}">
                <td class="text-center">{{ $user->name }}</td>
                <td class="font-w600">{{ $user->email }}</td>
                <td class="font-w600">{{ $user->role()->name }}</td>
                <td class="d-none d-sm-table-cell">
                  @if($user->state)
                    <span class="badge badge-success">Active</span>
                  @else
                    <span class="badge badge-danger">Inactive</span>
                  @endif
                </td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#roleModal" data-whatever="@getbootstrap" data-id="{{ $user->id }}" title="Modificar">
                      <i class="si si-pencil text-white"></i>
                
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $user->id }}" title="Eliminar">
                    <i class="si si-close text-white"></i>
                  
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Nuevo Usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="users" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <input type="hidden" name="table" value="User">
            <div class="form-group">
              <label for="role" class="col-form-label">Username</label>
              <input type="text" class="form-control" id="name" name="name" required placeholder="">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Email</label>
              <input type="email" class="form-control" id="email" name="email" required placeholder="">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Password</label>
              <input type="password" class="form-control" id="password" name="password" required placeholder="">
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Rol</label>
                <select class="js-select2 form-control" id="rol_id" name="rol_id" required style="width: 100%;" data-placeholder="Seleccione una opcion..">
                  @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Status</label>
                <select class="js-select2 form-control" id="state" name="state" required style="width: 100%;" data-placeholder="Seleccione una opcion..">
                  <option value="1">Active</option>
                  <option value="0">Inactive</option>
                </select>
            </div>
          </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Salir</a>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload');
        
    });


    $('.addnew').on('click',async function () {
      $('#label').html("Agregar Nuevo Usuario");
      $('#id').val('');
      $('#name').val('');
      $('#email').val('');
      $('#password').val('');
      $('#rol_id').val('');
      $('#state').val('');
    });

    $('.edit').on('click',async function () {
      $('#label').html("Editar Usuario");
      let id = $(this).data('id');
      $('#id').val(id);
      let users = @json($users);
      users.forEach((element) =>{
          if(id == element.id){
              $('#name').val(element.name);
              $('#email').val(element.email);
              $('#password').val(element.password);
              $('#rol_id').val(element.rol_id);
              $('#state').val(element.state);
          }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`users/${id}`,'delete',{table : 'User'});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

  </script>
@endsection