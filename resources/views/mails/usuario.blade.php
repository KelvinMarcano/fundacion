<!doctype html>
<html lang="es">
    <head>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8">
       
        <style>
        
         /* Font Definitions */
         @font-face
            {font-family:'Exo Light', arial;
            panose-1:2 4 5 3 5 4 6 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:arial;
            mso-font-pitch:variable;
            mso-font-signature:3 0 0 0 1 0;}
        </style>
    </head>

    <body style='tab-interval:35.4pt'>
         <a>
            <img src="{{ asset('images/faviconahora2.svg') }}" alt="" width="180px" height="180px" style="padding-top: 45px">
            </a>
        <div>
           <!--  <p style='text-align:center'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>From: (No Reply)</strong>
                </span>
            </p>
 -->
            <br>

            <p style='text-align:center'>
                <span style='font-size:12.0pt;line-height:107%'>
                    <strong>Activacion de Usuario</strong>
                </span>
            </p>

            <p>
             Hola Usuario {{ $name }} , por favor confirma tu correo clicando en el siguiente enlace
             
             <a href="">Activa mi cuenta</a>
            </p>

            <br>

          <!--   <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Este usuario sera activado en breve! </strong>
                </span>
            </p> -->
           
            <br>

            <p style='text-align:center'>This is an automatic notification issued by the
                <span style='font-size:12.0pt;line-height:107%;'>Fundacion Ahora</span>
                Application by Fundacion Ahora .
            </p>

        </div>

    </body>

</html>