@extends('layouts.backend')

@section('content')
 <div class="content">

    <div class="block block-rounded block-bordered">
      <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" href="#btabs-animated-fade-form" id="form">Nuevo Voluntarios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list" id="list">Listado de Volutarios</a>
        </li>
      </ul>
      <div class="block-content tab-content overflow-hidden">
           {{-- Tab 1--}}
          <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
              <form method="post" action="voluntarios" id="voluntarios">
                  {{ csrf_field() }}
                  <input type="hidden" id="id" name="id" value="">
                      <!-- Wizard Progress Bar -->
                  <div class="block block-rounded block-bordered">
                      <div class="block-header block-header-default">
                          <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fa fa-check"></i> Save
                            </button>
                            <button type="reset" class="btn btn-sm btn-outline-danger resetForm">
                            <i class="fa fa-repeat"></i> Reset
                            </button>
                          </div>
                        </div>
                        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                          <li class="nav-item">
                          <a class="nav-link" href="#wizard-progress-step0" id="tab">Datos Personal</a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link" href="#wizard-progress-step1" data-toggle="tab">Educacion Laboral</a>
                          </li>
                           <li class="nav-item">
                          <a class="nav-link" href="#wizard-progress-step2" data-toggle="tab">Direccion</a>
                          </li>
                           <li class="nav-item">
                          <a class="nav-link" href="#wizard-progress-step3" data-toggle="tab">Redes Sociales</a>
                          </li>
                        </ul> 
                        <div class="block-content block-content-full tab-content" style="min-height: 500px;">
                                <!-- TAB1.1 -->
                                <div class="tab-pane" id="wizard-progress-step0" role="tabpanel">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="nombre">Nombres*</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                                        </div>
                                    </div>
                                  
                                    <div class="form-group row">
                                      <label class="col-sm-3 col-form-label" for="apellido">Apellidos*</label>
                                      <div class="col-sm-8">
                                        <input type="text" class="form-control" name="apellido" placeholder="Apellido" required>
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label class="col-sm-3 col-form-label" for="doc_identidad">Doc. Identidad*</label>
                                      <div class="col-sm-8">
                                        <select id="doc_identidad" class="form-control" name="doc_identidad" placeholder="Documento de identidad" required>
                                          <option value='0'>Seleccionar Documento de identidad</option>
                                          <option value="CV">Cedula Venezolana</option>
                                            <option value="CE">Cedula Extranjero</option>
                                          <option value="PV">Pasaporte Venezolano</option>
                                          <option value="PE">Pasaporte Extranjero</option>
                                          <option value="CD">Carta Diplomatica</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label class="col-sm-3 col-form-label" for="num_doc_identidad" >Nº Documento*</label>
                                      <div class="col-sm-8">
                                        <input type="number" class="form-control" name="num_doc_identidad" placeholder="Numero de Documento de identidad" required>
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label class="col-sm-3 col-form-label" for="sexo">Sexo*</label>
                                      <div class="col-sm-8">
                                        <select id="sexo" class="form-control" name="sexo" placeholder="Sexo" required>
                                          <option value='0'>Seleccionar Sexo</option>
                                          <option value="Masculino">Masculino</option>
                                            <option value="Femenino">Femenino</option>
                                          <option value="Desconocido">Desconocido</option>
                         
                                        </select>
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <label class="col-sm-4 col-form-label" for="fecha_nacimiento">Fecha de Nacimiento*</label>
                                      <div class="col-sm-7">
                                        <input type="date" class="form-control" name="fecha_nacimiento" placeholder="Fecha de Nacimiento" required>
                                      </div>
                                    </div>  
                                </div>
                                <!-- TAB 2 -->      
                                <div class="tab-pane" id="wizard-progress-step1" role="tabpanel">
                                  <div class="form-group row">
                                    <label for="nacionalidad" class="col-sm-3 col-form-label">Nacionalidad*</label>
                                    <div class="col-sm-8">
                                    <select name="nacionalidad" id="nacionalidad" class="form-control" placeholder="Sexo" required>
                                    </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="num_telefono" class="col-sm-3 col-form-label">Telefono*</label>
                                    <div class="col-sm-8">
                                      <input type="tel" class="form-control" name="num_telefono" placeholder="0412-999-99-99" pattern="[0-9]{4}-[0-9]{3}-[0-9]{2}-[0-9]{2}" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="edo_civil" class="col-sm-3 col-form-label">Estado Civil*</label>
                                    <div class="col-sm-8">
                                      <select id="edo_civil" class="form-control" name="edo_civil" placeholder="Estado Civil" required>
                                        <option value='0'>Seleccionar Estado Civil</option>
                                        <option value="Soltero">Solter@</option>
                                          <option value="Casado">Casad@</option>
                                        <option value="Divorciado">Divorciad@</option>
                                        <option value="Viudo">Viud@</option>
                                        <option value="Concubinato">Concubinato</option>
                       
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="nivel_estudio" class="col-sm-3 col-form-label">Nivel de Estudio*</label>
                                    <div class="col-sm-8">
                                    <select id="edo_civil" class="form-control" name="nivel_estudio" placeholder="Nivel de estudio" required>
                                        <option value='0'>Seleccionar Nivel de Estudio</option>
                                        <option value="No Aplica">No Aplica</option>
                                          <option value="Primaria">Primaria</option>
                                        <option value="Bachiller">Bachiller</option>
                                        <option value="Superior">Superior</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="profesion" class="col-sm-3 col-form-label">Profesion</label>
                                    <div class="col-sm-8">
                                      <select name="profesion" id="profesion" class="form-control" placeholder="Profesion" required>                              
              
                                      </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="situacion_laboral" class="col-sm-3 col-form-label">Situacion Laboral*</label>
                                    <div class="col-sm-8">
                                      <select id="situacion_laboral" class="form-control" name="situacion_laboral" placeholder="Situacion Laboral" required>
                                        <option value='0'>Seleccionar Situacion Laboral</option>
                                        <option value="Trabajando">Trabajando</option>
                                          <option value="Desempleado">Desempleado</option>
                                        <option value="Am@ de casa">Am@ de casa</option>
                                        <option value="Estudiante">Estudiante</option>
                                        <option value="Pensionado o Jubilado">Pensionado o Jubilado</option>
                       
                                      </select>
                                    </div>
                                  </div>  
                                </div>

                                <!-- TAB 3 -->      
                                <div class="tab-pane" id="wizard-progress-step2" role="tabpanel">
                                  <div class="form-group row">
                                    <label for="pais" class="col-sm-3 col-form-label">Pais*</label>
                                    <div class="col-sm-8">
                                    <select name="pais" id="pais" class="form-control" placeholder="Pais" required>
                                  
                                      
                                    </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="estado" class="col-md-3 control-label">Estado</label>
                                    <div class="col-sm-8">
                                    <select name="estado" id="estado" class="form-control" placeholder="Estado" required>
                                      
                                    </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="municipio" class="col-sm-3 col-form-label">Municipio*</label>
                                    <div class="col-sm-8">
                                    <select name="municipio" id="municipio" class="form-control" placeholder="Municipio" required>
                                      
                                    </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="parroquia" class="col-sm-3 col-form-label">Parroquia*</label>
                                    <div class="col-sm-8">
                                    <select name="parroquia" id="parroquia" class="form-control" placeholder="Parroquia" required>
                                      
                                    </select>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="direccion" class="col-sm-3 col-form-label">Direccion*</label>
                                    <div class="col-sm-8">
                                      <textarea rows="10" cols="50" name="direccion" class="form-control">Agregue Direccion ...</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="cargo_comunidad" class="col-sm-3 col-form-label">Cargo en la Comunidad</label>
                                    <div class="col-sm-8">
                                    <select id="cargo_comunidad" class="form-control" name="cargo_comunidad" placeholder="Cargo en la Comunidad" required>
                                      <option value='0'>Seleccionar Cargo en la Comunidad</option>
                                      <option value="Jefe de Calle">Jefe de Calle</option>
                                        <option value="Lider de Comunidad">Lider de Comunidad</option>
                                      <option value="Junta de Condominio">Junta de Condominio</option>
                                      <option value="Junta Vecinal">Junta Vecinal</option>
                                      <option value="Coordinador">Coordinador</option>
                                      <option value="Vocero">Vocero</option>
                                      <option value="Vecino">Vecino</option>
                                    </select>
                                    </div>
                                  </div>    
                                </div>

                                <!-- TAB 4 -->  
                                <div class="tab-pane" id="wizard-progress-step3" role="tabpanel">
                                  <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-8">
                                      <input type="email" class="form-control" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>" required>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label for="instagram" class="col-sm-3 col-form-label">Instagram</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" name="instagram" placeholder="Instagram">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row">
                                    <label for="facebook" class="col-sm-3 col-form-label">Facebook</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" name="facebook" placeholder="Facebook">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row">
                                    <label for="twitter" class="col-sm-3 col-form-label">Twitter</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" name="twitter" placeholder="Twitter">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row">
                                    <label for="modalidad_ayuda" class="col-sm-3 col-form-label">Modalidad de Ayuda</label>
                                    <div class="col-sm-8">
                                      <p><input type="checkbox" name="modalidad_ayuda[]" value="De Campo" /> De Campo</p>
                                      <p><input type="checkbox" name="modalidad_ayuda[]" value="Desde Casa" /> Desde Casa</p>
                                      
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label for="movilidad" class="col-sm-3 col-form-label">Movilidad*</label>
                                    <div class="col-sm-8">
                                        <select id="movilidad" class="form-control" name="movilidad" placeholder="Movilidad" required>
                                          <option value='0'>Seleccionar Movilidad en la Comunidad</option>
                                          <option value="Particular">Particular</option>
                                            <option value="Transporte Publico">Transporte Publico</option>
                                          <option value="A Pie">A Pie</option>
                                          
                                        </select>
                                    </div>

                                  </div>

                                  <div class="form-group row">
                                    <label for="comentario" class="col-sm-3 col-form-label">Comentarios</label>
                                    <div class="col-sm-8">
                                      <textarea rows="6" cols="50" name="comentario" class="form-control">Agregue Comentarios ...</textarea>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label for="acepto" class="col-md-3 control-label"> </label>
                                    <div class="col-md-9">
                                      <p><input type="checkbox" name="acepto[]" value="S" /> Si, Acepto</p>
                                    </div>
                                  </div>

                                  <div class="custom-control custom-checkbox custom-control-primary">
                                        <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" data-toggle="modal" data-target="#modal-terms">
                                  </div>               
                                </div>
                        </div>
                  </div>
              </form>
          </div> 
      </div>  

            
      


          {{-- Tab 2--}}
           <div class="tab-pane fade" id="btabs-animated-fade-list" role="tabpanel" style="overflow-x: auto;">    
              <table class="table table-responsive table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
                  <thead>
                      <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apellidos</th>
                        <th class="text-center">Email</th>
                        <th class="text-center"  style="width: 20%;">Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($voluntarios as $voluntario)
                      <tr class="text-center" id="trrol_{{ $voluntario->id }}">
                        <td class="text-center">{{ $voluntario->nombres }}</td>
                        <td class="font-w600">{{ $voluntario->firstname }}</td>
                        <td class="text-center">{{ $voluntario->lastname }}</td>
                        <td class="d-none d-sm-table-cell">
                          <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="tooltip" data-animation="true" data-placement="top" data-whatever="@getbootstrap" data-id="{{ $voluntario->id }}" onclick="editEmploye(this);">
                              <i class="si si-pencil text-white"></i>
                              Edit
                          </button>
                          <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $voluntario->id }}">
                            <i class="si si-close text-white"></i>
                            Delete
                          </button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> 
               {{-- FIN Tab 2--}}
        </div>  
    </div>
</div>

<script>

     $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload');

      //     $("#pais").change(function () {

      //     $('#estado').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
      //     $('#municipio').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
      //     $('#parroquia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
          
          
      //     $("#pais option:selected").each(function () {
      //       id = $(this).val();
      //       $.post("includes/getEstado.php", { id: id }, function(data){
      //         $("#estado").html(data);
      //       });            
      //     });
      //   })
      // });
      
      // $(document).ready(function(){
      //   $("#estado").change(function () {
      //     $('#municipio').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
      //     $('#parroquia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
          
          
      //     $("#estado option:selected").each(function () {
      //       id2 = $(this).val();
      //       $.post("includes/getMunicipio.php", { id: id2 }, function(data){
      //         $("#municipio").html(data);
      //       });            
      //     });
      //   })
      // });

      // $(document).ready(function(){
      //   $("#municipio").change(function () {
      //     $('#parroquia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
          
          
      //     $("#municipio option:selected").each(function () {
      //       id3 = $(this).val();
      //       $.post("includes/getParroquia.php", { id: id3 }, function(data){
      //         $("#parroquia").html(data);
      //       });            
      //     });
      //   })
      // });

      
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Nuevo Voluntario");
      // $('#id').val('');
      // $('#code').val('');
      // $('#firstname').val('');
      // $('#lastname').val('');
      // $('#sex').val('');
      // $('#birthdate').val('');
      // $('#address').val('');
      // $('#numbersocial').val('');
      // $('#zipcode').val('');
      // $('#numberphone').val('');
      // $('#numberhome').val('');
      // $('#numberfax').val('');
      // $('#sociallink').val('');
      // $('#departamento').val('');
      // $('#idsite').val('');
      // $('#horario').val('');
      // $('#cargo').val('');
      // $('#profesion').val('');
      // $('#roles').val('');
      // $('#status').val('');


    });

    async function editEmploye(sender){
      $("#form").trigger("click");
      let id = $(sender).data('id');
      $('#id').val(id);
      let voluntario = @json($voluntarios);
      voluntario.forEach((element) =>{
        if(id == element.id){
          // $('#code').val(element.code);
          // $('#firstname').val(element.firstname);
          // $('#lastname').val(element.lastname);
          // $('#sex').val(element.sex);
          // $('#birthdate').val(element.birthdate);
          // $('#address').val(element.address);
          // $('#numbersocial').val(element.numbersocial);
          // $('#zipcode').val(element.zipcode);
          // $('#numberphone').val(element.numberphone);
          // $('#numberhome').val(element.numberhome);
          // $('#sociallink').val(element.sociallink);
          // $('#departamento').val(element.departamentid);
          // $('#idsite').val(element.idsite);
          // $('#horario').val(element.scheduleid);
          // $('#cargo').val(element.positionid);
          // $('#profesion').val(element.professionid);
          // $('#roles').val(element.rolesid);
          // $('#status').val(element.status);
         
        }
      });      
    }

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`voluntarios/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });


     function HandleBackFunctionality(e){
        if(window.event){
          if(window.event.clientX < 40 && window.event.clientY < 0){
          }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });
         }
     }
     else{
        if(event.currentTarget.performance.navigation.type == 1){
         }
         if(event.currentTarget.performance.navigation.type == 2){
        }
     }
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }
  </script>

@endsection