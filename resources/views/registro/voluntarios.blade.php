@extends('layouts.backend')

@section('content')
 <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Voluntarios</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#roleModal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Nuevo Voluntario
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                 <th class="text-center">ID</th>
                <th class="text-center">Nombres</th>
                <th class="text-center">Apellidos</th>
                <th class="text-center">Sexo</th>
                <th class="text-center">Edad</th>
                <th class="text-center">Teléfono</th>
                <th class="text-center">Email</th>
                <th class="text-center"  style="width: 20%;">Acciones</th>
              </tr>
          </thead>
          <tbody>
           @foreach($voluntarios as $voluntario)
              <tr class="text-center" id="trrol_{{ $voluntario->id }}">
                 <td class="text-center">{{ $voluntario->id }}</td>
                <td class="text-center">{{ $voluntario->nombre }}</td>
                <td class="font-w600">{{ $voluntario->apellido }}</td>
                <td class="text-center">{{ $voluntario->sexo }}</td>
                <td class="font-w600">{{ $voluntario->fecha_nacimiento }}</td>
                <td class="text-center">{{ $voluntario->num_telefono }}</td>
                <td class="text-center">{{ $voluntario->email }}</td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-placement="top" data-whatever="@getbootstrap" data-id="{{ $voluntario->id }}" data-toggle="modal" data-target="#roleModal" title="Modificar">
                      <i class="si si-pencil text-white"></i>
                     
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $voluntario->id }}" title="Eliminar">
                    <i class="si si-close text-white"></i>
                  
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

   {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Nuevo Voluntario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="post" action="voluntarios" id="voluntarios">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <input type="hidden" name="table" value="voluntarios">
            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
              <li class="nav-item">
               <a class="nav-link active" href="#btabs-animated-fade-form" id="form">Datos Personales</a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="#wizard-progress-step1" data-toggle="tab">Educación Laboral</a>
              </li>
               <li class="nav-item">
              <a class="nav-link" href="#wizard-progress-step2" data-toggle="tab">Dirección</a>
              </li>
               <li class="nav-item">
              <a class="nav-link" href="#wizard-progress-step3" data-toggle="tab">Redes Sociales</a>
              </li>
            </ul> 
            <div class="block-content block-content-full tab-content" style="min-height: 700px;">
                    <!-- TAB1.1 -->
                  <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="nombre">Nombres*</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                            </div>
                        </div>
                      
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="apellido">Apellidos*</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="doc_identidad">Doc. Identidad*</label>
                          <div class="col-sm-8">
                            <select  class="form-control" id="doc_identidad" name="doc_identidad" placeholder="Documento de identidad" required>
                              <option value='0'>Seleccionar Documento de identidad</option>
                              <option value="CV">Cedula Venezolana</option>
                                <option value="CE">Cedula Extranjero</option>
                              <option value="PV">Pasaporte Venezolano</option>
                              <option value="PE">Pasaporte Extranjero</option>
                              <option value="CD">Carta Diplomatica</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="num_doc_identidad" >Nº Documento*</label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" id="num_doc_identidad" name="num_doc_identidad" placeholder="Numero de Documento de identidad" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="sexo">Sexo*</label>
                          <div class="col-sm-8">
                            <select id="sexo" class="form-control" name="sexo" placeholder="Sexo" required>
                              <option value='0'>Seleccionar Sexo</option>
                              <option value="Masculino">Masculino</option>
                                <option value="Femenino">Femenino</option>
                              <option value="Desconocido">Desconocido</option>
             
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label" for="fecha_nacimiento">Fecha de Nacimiento*</label>
                          <div class="col-sm-7">
                            <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento"  placeholder="Fecha de Nacimiento" required>
                          </div>
                        </div>

                        <div class="form-group row">
                            <label for="nacionalidad" class="col-sm-3 col-form-label">Nacionalidad*</label>
                            <div class="col-sm-8">
                            <select name="nacionalidad" id="nacionalidad" class="form-control" placeholder="Nacionalidad" required>
                                @foreach($nacionalidad as $nacionalidad)
                                    <option value="{{ $nacionalidad->id }}">{{ $nacionalidad->nacionalidad }}</option>
                                  @endforeach
                            </select>
                                  
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="num_telefono" class="col-sm-3 col-form-label">Telefono*</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="num_telefono" name="num_telefono" placeholder="0412-999-99-99" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="edo_civil" class="col-sm-3 col-form-label">Estado Civil*</label>
                            <div class="col-sm-8">
                              <select id="edo_civil" class="form-control" name="edo_civil" placeholder="Estado Civil" required>
                                <option value='0'>Seleccionar Estado Civil</option>
                                <option value="Soltero">Solter@</option>
                                  <option value="Casado">Casad@</option>
                                <option value="Divorciado">Divorciad@</option>
                                <option value="Viudo">Viud@</option>
                                <option value="Concubinato">Concubinato</option>
               
                              </select>
                            </div>
                          </div>
                    </div>
                    <!-- TAB 2 -->      
                    <div class="tab-pane" id="wizard-progress-step1" role="tabpanel">
                      
                      <div class="form-group row">
                        <label for="nivel_estudio" class="col-sm-3 col-form-label">Nivel de Estudio*</label>
                        <div class="col-sm-8">
                        <select class="form-control" id="nivel_estudio" name="nivel_estudio" placeholder="Nivel de estudio" required>
                            <option value='0'>Seleccionar Nivel de Estudio</option>
                            <option value="No Aplica">No Aplica</option>
                              <option value="Primaria">Primaria</option>
                            <option value="Bachiller">Bachiller</option>
                            <option value="Superior">Superior</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="profesion" class="col-sm-3 col-form-label">Profesion</label>
                        <div class="col-sm-8">
                          <select name="profesion" id="profesion" class="form-control" placeholder="Profesion" required>                  
                              @foreach($profesiones as $profesion)
                                <option value="{{ $profesion->id }}">{{ $profesion->descripcion1 }}</option>
                              @endforeach                

                          </select>
                        
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="situacion_laboral" class="col-sm-3 col-form-label">Situacion Laboral*</label>
                        <div class="col-sm-8">
                          <select id="situacion_laboral" class="form-control" name="situacion_laboral" placeholder="Situacion Laboral" required>
                            <option value='0'>Seleccionar Situacion Laboral</option>
                            <option value="Trabajando">Trabajando</option>
                              <option value="Desempleado">Desempleado</option>
                            <option value="Am@ de casa">Am@ de casa</option>
                            <option value="Estudiante">Estudiante</option>
                            <option value="Pensionado o Jubilado">Pensionado o Jubilado</option>
           
                          </select>
                        </div>
                      </div>  
                    </div>

                    <!-- TAB 3 -->      
                    <div class="tab-pane" id="wizard-progress-step2" role="tabpanel">
                      <div class="form-group row">
                        <label for="pais" class="col-sm-3 col-form-label">Pais*</label>
                        <div class="col-sm-8">
                        <select name="pais" id="pais" class="form-control" placeholder="Pais" onchange="changePais(this)" required>
                           <option value='0'>Seleccionar</option>
                               @foreach($paises as $paise)
                                <option value="{{ $paise->id }}">{{ $paise->nombre }}</option>
                              @endforeach
                          
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="estado" class="col-md-3 control-label">Estado*</label>
                        <div class="col-sm-8">
                        <select name="estado" id="estado" class="form-control" placeholder="Estado" onchange="changeEstado(this)" required>
                           <option value='0'>Seleccionar</option>
                              @foreach($estado as $estad)
                                <option value="{{ $estad->id }}">{{ $estad->nombre }}</option>
                              @endforeach
                        </select>
                        
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="municipio" class="col-sm-3 col-form-label">Municipio*</label>
                        <div class="col-sm-8">
                        <select name="municipio" id="municipio" class="form-control" placeholder="Municipio" onchange="changeMunicipio(this)" required>
                           <option value='0'>Seleccionar</option>
                             @foreach($municipio as $municip)
                                <option value="{{ $municip->id }}">{{ $municip->nombre }}</option>
                              @endforeach
                        </select>
                        
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="parroquia" class="col-sm-3 col-form-label">Parroquia*</label>
                        <div class="col-sm-8">
                        <select name="parroquia" id="parroquia" class="form-control" placeholder="Parroquia" required>
                           <option value='0'>Seleccionar</option>
                          @foreach($parroquia as $parroq)
                            <option value="{{ $parroq->id }}">{{ $parroq->nombre }}</option>
                          @endforeach
                        </select>
                        
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="direccion" class="col-sm-3 col-form-label">Direccion*</label>
                        <div class="col-sm-8">
                          <textarea rows="10" cols="50" name="direccion" id="direccion" class="form-control">Agregue Direccion ...</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cargo_comunidad" class="col-sm-3 col-form-label">Cargo en la Comunidad</label>
                        <div class="col-sm-8">
                        <select id="cargo_comunidad" class="form-control" name="cargo_comunidad" id="cargo_comunidad" placeholder="Cargo en la Comunidad">
                          <option value='0'>Seleccionar Cargo en la Comunidad</option>
                          <option value="Jefe de Calle">Jefe de Calle</option>
                            <option value="Lider de Comunidad">Lider de Comunidad</option>
                          <option value="Junta de Condominio">Junta de Condominio</option>
                          <option value="Junta Vecinal">Junta Vecinal</option>
                          <option value="Coordinador">Coordinador</option>
                          <option value="Vocero">Vocero</option>
                          <option value="Vecino">Vecino</option>
                        </select>
                        </div>
                      </div>    
                    </div>

                    <!-- TAB 4 -->  
                    <div class="tab-pane" id="wizard-progress-step3" role="tabpanel">
                      <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="instagram" class="col-sm-3 col-form-label">Instagram</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram">
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label for="facebook" class="col-sm-3 col-form-label">Facebook*</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook" required>
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label for="twitter" class="col-sm-3 col-form-label">Twitter</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter">
                        </div>
                      </div>

                      
                      <div class="form-group row">
                        <label for="modalidad_ayuda" class="col-sm-3 col-form-label">Modalidad de Ayuda</label>
                        <div class="col-sm-8">
                          <p><input type="radio" id="modalidad_ayuda" name="modalidad_ayuda" value="1"/> De Campo</p>
                          <p><input type="radio" id="modalidad_ayuda" name="modalidad_ayuda" value="2"/> Desde Casa</p>
                          
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="movilidad" class="col-sm-3 col-form-label">Movilidad*</label>
                        <div class="col-sm-8">
                            <select id="movilidad" class="form-control" name="movilidad" placeholder="Movilidad" required>
                              <option value='0'>Seleccionar Movilidad en la Comunidad</option>
                              <option value="Particular">Particular</option>
                                <option value="Transporte Publico">Transporte Publico</option>
                              <option value="A Pie">A Pie</option>
                              
                            </select>
                        </div>

                      </div>

                      <div class="form-group row">
                        <label for="comentario" class="col-sm-3 col-form-label">Comentarios</label>
                        <div class="col-sm-8">
                          <textarea rows="6" cols="50" id="comentario" name="comentario" class="form-control">Agregue Comentarios ...</textarea>
                        </div>
                      </div>
                      <div  class="col-4" style="text-align: -webkit-center;margin: auto;">
                          <div class="custom-control custom-checkbox custom-control-primary">
                            <input type="checkbox" class="custom-control-input" id="acepto_terminos" name="acepto_terminos" value="1" required>
                            <label class="custom-control-label" for="acepto_terminos" data-toggle="modal" data-target="#modal-terms">Si, Acepto (Ver TYC)</label>
                          </div>
                        </div>
                    </div>
                     <div class="modal-footer">
                      <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Salir</a>
                      <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
              </div>
             </div> 
        </form>
      </div>
    </div>
  </div>
  <!-- Terms Modal -->
    <div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Terminos &amp; Condiciones</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                      <iframe src="{{ asset('media/terminos.pdf') }}" title="Terminos" width="450" height="450"></iframe>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
<script>

     $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload');
     
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Nuevo Voluntario");
      $('#id').val('');
      $('#nombre').val('');
      $('#apellido').val('');
      $('#doc_identidad').val('');
      $('#num_doc_identidad').val('');
      $('#sexo').val('');
      $('#fecha_nacimiento').val('');
      $('#nacionalidad').val('');
      $('#num_telefono').val('');
      $('#edo_civil').val('');
      $('#nivel_estudio').val('');
      $('#profesion').val('');
      $('#situacion_laboral').val('');
      $('#pais').val('');
      $('#estado').val('');
      $('#municipio').val('');
      $('#parroquia').val('');
      $('#direccion').val('');
      $('#cargo_comunidad').val('');
      $('#email').val('');
      $('#instagram').val('');
      $('#facebook').val('');
      $('#twitter').val('');
      $('#modalidad_ayuda').val('');
      $('#movilidad').val('');
      $('#comentario').val('');
      $('#acepto_terminos').val('');


    });

    $('.edit').on('click',async function () {
        $('#label').html("Editar Voluntario");
      let id = $(this).data('id');
      $('#id').val(id);
      let voluntario = @json($voluntarios);
      voluntario.forEach((element) =>{
        if(id == element.id){
          $('#nombre').val(element.nombre);
          $('#apellido').val(element.apellido);
          $('#doc_identidad').val(element.doc_identidad);
          $('#num_doc_identidad').val(element.num_doc_identidad);
          $('#sexo').val(element.sexo);
          $('#fecha_nacimiento').val(element.fecha_nacimiento);
          $('#nacionalidad_id').val(element.nacionalidad_id);
          $('#num_telefono').val(element.num_telefono);
          $('#edo_civil').val(element.edo_civil);
          $('#nivel_estudio').val(element.nivel_estudio);
          $('#profesion').val(element.profesion_id);
          $('#situacion_laboral').val(element.situacion_laboral);
          $('#pais').val(element.pais_id);
          $('#estado').val(element.estado_id);
          $('#municipio').val(element.municipo_id);
          $('#parroquia').val(element.parroquia_id);
          $('#direccion').val(element.direccion);
          $('#cargo_comunidad').val(element.cargo_comunidad);
          $('#email').val(element.email);
          $('#instagram').val(element.instagram);
          $('#facebook').val(element.facebook);
          $('#twitter').val(element.twitter);
         
          if(element.modalidad_ayuda == 2){
             $("input:radio[value='2']").prop('checked',true);
          }else{
             $("input:radio[value='1']").prop('checked',true);
          }

          $('#movilidad').val(element.movilidad);
          $('#comentario').val(element.comentarios);
          
          if(element.acepto_terminos == 1){
           console.log(element.acepto_terminos);
            $("input:checkbox[value='1']").prop('checked',true);
          }else{
            //console.log(element.acepto_terminos);
            $("input:checkbox[value='']").prop('checked',false);
          }
          //$('#acepto_terminos').val(element.acepto_terminos);
         
        }
      });      
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`voluntarios/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });


     function HandleBackFunctionality(e){
        if(window.event){
          if(window.event.clientX < 40 && window.event.clientY < 0){
          }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });
         }
     }
     else{
        if(event.currentTarget.performance.navigation.type == 1){
         }
         if(event.currentTarget.performance.navigation.type == 2){
        }
     }
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

   function changePais(sender) {  

    let estad = @json($estado);
    console.log(estad);
    $('#estado option').remove();
    estad.forEach((element) =>{
      if($(sender).find(':selected').val() == element.pais_id){
        $('#estado').append(`<option value="${element.id}">${element.nombre}</option>}`);
        
      }
    });
   
  }


   function changeEstado(sender) {  

      let munic = @json($municipio);
      console.log(munic);
      $('#municipio option').remove();
      munic.forEach((element) =>{
        if($(sender).find(':selected').val() == element.estado_id){
          $('#municipio').append(`<option value="${element.id}">${element.nombre}</option>}`);
          
        }
      });
     
    }

     function changeMunicipio(sender) {  

      let parroq = @json($parroquia);
      console.log(parroq);
      $('#parroquia option').remove();
      parroq.forEach((element) =>{
        if($(sender).find(':selected').val() == element.municipio_id){
          $('#parroquia').append(`<option value="${element.id}">${element.nombre}</option>}`);
          
        }
      });
     
    }
  </script>

@endsection