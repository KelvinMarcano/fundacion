@extends('layouts.backend')

@section('content')
<!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Fundacion Ahora</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item">Layout</li>
                        <li class="breadcrumb-item">Content</li> -->
                        <li class="breadcrumb-item active" aria-current="page">Menu Principal</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      
    </div>
    <!-- END Page Content -->
@endsection
<!-- <style type="text/css">
:root {
    --bg: #1a1e24;
    --color: #eee;
}

.wrapper {
    padding: 1.5rem 0;
    filter: url('#goo');
}

.button {
    display: inline-block;
    text-align: center;
    background: var(--color);
    color: var(--bg);
    font-weight: bold;
    padding: 1.18em 1.32em 1.03em;
    line-height: 1;
    border-radius: 1em;
    position: relative;
    min-width: 8.23em;
    text-decoration: none;
    font-family: var(--font);
    font-size: 1.25rem;
}

.button:before,
.button:after {
    width: 4.4em;
    height: 2.95em;
    position: absolute;
    content: "";
    display: inline-block;
    background: var(--color);
    border-radius: 50%;
    transition: transform 1s ease;
    transform: scale(0);
    z-index: -1;
}

.button:before {
    top: -25%;
    left: 20%;
}

.button:after {
    bottom: -25%;
    right: 20%;
}

.button:hover:before,
.button:hover:after {
    transform: none;
}


/* Demo styles */

body {
    width: 100%;
    height: 100%;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--bg)
}
</style>
 -->